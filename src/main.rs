use std::env;
use std::fs::File;
use std::io::Read;
use yaml_rust::YamlLoader;
use project_template::error;

fn usage() {
    println!("Project Template");
    println!("\tproject_template <path to project template yaml file>");
}

fn run_app() -> Result<(), error::PTError> {
    let file_path = env::args().nth(1).ok_or_else(|| error::PTError::new("You must provide a file path.\n\tproject_template <file_path>", 1))?;
    let mut file = File::open(file_path).map_err(|_| error::PTError::new("File not found. Provide a project template file path.", 1))?;
    let mut file_content = String::new();
    file.read_to_string(&mut file_content).map_err(|_| error::PTError::new("Cannot read the file. Provide a project template file path", 1))?;
    let project_template = YamlLoader::load_from_str(&file_content)
        .map_err(|_| error::PTError::new("Cannot properly read the passed file as a Yaml file.", 1))?
        .pop().ok_or_else(|| error::PTError::new("The file is not well formed. Check the documentation about how to write project template file.", 1))?
        .into_hash().ok_or_else(|| error::PTError::new("The file is not well formed. Check the documentation about how to write project template file.", 1))?;

    // in un for project template viene scompattato in <Yaml, Yaml>
    project_template::parse_entry(project_template, ".".to_string())?;
    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 1 {
        usage();
    } else {
        match &args[1][..] {
            "-h" => usage(),
            "--help" => usage(),
            _ => {std::process::exit(match run_app() {
                Ok(_) => 0,
                Err(e) => {
                    eprintln!("{}", e);
                    e.get_error_number()
                }
            });
            }
        }
    }
}
