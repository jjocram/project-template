use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct PTError {
    error_msg: String,
    error_number: i32,
}

impl PTError {
    pub fn new(msg: &str, number: i32) -> PTError {
        PTError {
            error_msg: msg.to_string(),
            error_number: number
        }
    }

    pub fn get_error_number(&self) -> i32 {
        self.error_number
    }
}

impl fmt::Display for PTError {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.error_msg)
    }
}

impl Error for PTError {
    fn description(&self) -> &str {
        &self.error_msg
    }
}
