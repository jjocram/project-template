use std::fs::{File, create_dir_all};
use yaml_rust::yaml::Hash;

pub mod error;

pub fn parse_entry(mut hash: Hash, base_path: String) -> Result<(), error::PTError>{
    for entry in hash.entries(){
        let directory = entry.key().clone().into_string().ok_or_else(|| error::PTError::new("Cannot convert the directory name into a string. Check the documentation about how to write project template files.", 1))?;
        let complete_path = [base_path.clone(), "/".to_string(), directory.clone()].join("");

        let contents = entry.get().clone().into_vec().ok_or_else(|| error::PTError::new("Cannot read the directory contents. Check the documentation about how to write project template files.", 1))?;
        
        for el in contents {
            if el.as_str().is_some() {
                // TODO: create file
                create_dir_all(complete_path.clone()).map_err(|_| error::PTError::new(format!("Cannot create path {}.", complete_path).as_str(), 1))?;
                let file_name = el.into_string().ok_or_else(|| error::PTError::new("Cannot convert file name to string.", 1))?;
                File::create([complete_path.clone(), "/".to_string(), file_name.clone()].join("")).map_err(|_| error::PTError::new(format!("Cannot create file {}.", file_name.clone()).as_str(), 1))?;
            }
            else if el.as_hash().is_some() {
                // TODO: create a directory and parse the sub sections of yaml file
                let inner_hash = el.into_hash().ok_or_else(|| error::PTError::new("The file is not well formed. Check the documentation about how to write project template file.", 1))?;
                parse_entry(inner_hash, complete_path.clone())?;
            }
            else {
                return Err(error::PTError::new("Cannot analyze the file. Check the documentation about how to write project template file.", 1));
            }
        }
    }

    Ok(())
}
